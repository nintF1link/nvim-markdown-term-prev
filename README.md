# nvim-markdown-term-prev

This is a simple plugin for NeoVim which opens a md file preview in another nvim's window, this plugin depends of `tty-markdown` gem